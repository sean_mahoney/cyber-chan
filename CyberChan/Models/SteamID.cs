﻿namespace CyberChan.Models
{
    internal class SteamId
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
